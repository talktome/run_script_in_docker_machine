:: This batch file works in Windows and requires Docker Machine and Putty.
::
:: When used in a directory descendant of '%homedrive%\Users', it connects to the
:: default Docker Machine and runs a Linux shell script with identical name to this
:: file (excluding ending), provided it exists.
::
:: I.e., if the name of this file is 'file.cmd', it connects to the Docker Machine
:: "Default" and runs 'name'.
::
:: Thus you can copy and rename this file to run arbitrary script.
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:: Environment variables are local to this file.
setlocal

:: Start Docker Machine, if not running.
for /f "tokens=*" %%a in ('docker-machine.exe status default') do @set status=%%a
if not %status%==Running docker-machine.exe restart default

:: Get docker machine IP.
for /f "tokens=*" %%a in ('docker-machine.exe ip default') do @set ip=%%a

:: Generate one-line command script to call other script.
set file=%~n0
set pwd=%~dp0
set pwd=%pwd:C:=/c%
set pwd=%pwd:\=/%
set command=%tmp%\%random%%random%%random%.tmp
echo "%pwd%%file%" > %command%

:: Use putty to SSH into Docker Machine, and pass command script that runs other script.
start /b putty docker@%ip% -pw tcuser -m %command% -t