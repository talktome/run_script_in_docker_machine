Run script in Docker Machine
============================

Here is a small batch file I use for running shell scripts in the default Docker Machine on Windows, from Windows.

Requirements
------------

[Docker Toolbox] and [Putty].

Installation
------------

In a directrory descendant of of *%homedrive%\Users*:

    git clone https://bitbucket.org/talktome/run_script_in_docker_machine

Usage
-----

Run *sh.cmd* to try. Check source to understand how to use. No editing needed; just copy and rename.

Feedback
--------

Any feedback is much appreciated. Use Bitbucket for that.

[Docker Toolbox]: https://www.docker.com/toolbox
[Putty]: http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html